#!/bin/bash

NewHash = $(getent passwd | cut -d: -f1,6 | md5sum)

OldHash = $(head -2 /var/log/current_users | tail -1)

now=$(date)

if [$NewHash -ne $OldHash]

  then 
  
 echo "$now" > /var/log/current_users
 getent passwd | cut -d: -f1,6 | md5sum >> /var/log/current_users

 fi